unit Unit19;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.DBCtrls, Vcl.Grids,
  Vcl.DBGrids, Datasnap.DBClient, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, Vcl.StdCtrls, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.ExtCtrls, Vcl.Imaging.jpeg;

type
  TForm19 = class(TForm)
    Fish: TClientDataSet;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    Button1: TButton;
    FishSpeciesNo: TFloatField;
    FishCategory: TStringField;
    FishCommon_Name: TStringField;
    FishSpeciesName: TStringField;
    FishLengthcm: TFloatField;
    FishLength_In: TFloatField;
    FishNotes: TMemoField;
    FishGraphic: TGraphicField;
    Memo1: TMemo;
    NetHTTPRequest1: TNetHTTPRequest;
    NetHTTPClient1: TNetHTTPClient;
    DBMemo1: TDBMemo;
    Button2: TButton;
    Image1: TImage;
    Button3: TButton;
    DBImage1: TDBImage;
    cbRegistered: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FishAfterScroll(DataSet: TDataSet);
    procedure cbRegisteredClick(Sender: TObject);
  private
    { Private declarations }
    function DataFileName : string;
    procedure GetFishImage;

    function BuildURL : string;
    function Get(URL : string) : string;
    procedure Log(const s : string);
  public
    { Public declarations }
  end;

  TImprovedJPEG = class(TJpegImage)
  public
    class function CanLoadFromStream(Stream: TStream): Boolean; override;
  end;

var
  Form19: TForm19;

implementation

uses
  System.Generics.Collections,
  Vcl.Imaging.JConsts,
  vcl.Imaging.GIFImg,
  System.Json,
  System.RegularExpressions;

{$R *.dfm}

function TForm19.BuildURL: string;
begin
  var URL := 'https://commons.wikimedia.org/w/api.php?action=opensearch&search=' + StringReplace(FishSpeciesName.AsString, ' ', '+', [rfReplaceAll]);
  var ResponseText := Get(URL);
  Memo1.Text := ResponseText;
  var ResponseArray : TJSONArray := TJSONObject.ParseJSONValue(ResponseText) as TJSONArray;
  var URLArray : TJSONArray := ResponseArray[3] as TJSONArray;

  if URLArray.Count > 0 then
    Result := URLArray[0].AsType<string>
  else
    Exit('');
end;

procedure TForm19.Button1Click(Sender: TObject);
begin
  Fish.First;
  while not Fish.Eof do
  begin
    try
      try
        GetFishImage;
      finally
        Fish.Next;
      end;
    except;

    end;
  end;
end;

procedure TForm19.Button2Click(Sender: TObject);
begin
  GetFishImage;
end;

procedure TForm19.cbRegisteredClick(Sender: TObject);
begin
  if cbRegistered.Checked then
    TPicture.RegisterFileFormat('XYZ', 'Not a format', TImprovedJPEG)
  else
    TPicture.UnregisterGraphicClass(TImprovedJPEG);
end;

function TForm19.DataFileName: string;
begin
  result := ChangeFileExt(Application.ExeName, '.xml');
end;

procedure TForm19.FishAfterScroll(DataSet: TDataSet);
var
  s : TStream;
  jpg : TJpegImage;
begin
  //TDBImage is having difficulty recognising JPGs
  if FishGraphic.IsNull then
    Exit;
  try
    s := Fish.CreateBlobStream(FishGraphic, bmRead);
    s.Position := 0;
    try
      Image1.Picture.LoadFromStream(s);
    except
      Log('Failed to LoadFromStream ' + FishCommon_Name.AsString);
      s.Position := 0;
      jpg := TJpegImage.Create;
      jpg.LoadFromStream(s);
      Image1.Picture.Assign(jpg);
      jpg.Free;
    end;
  except;
    Log('Failed JPG Load ' + FishCommon_Name.AsString);
    Image1.Picture := nil;
  end;
end;

procedure TForm19.FormCreate(Sender: TObject);
begin
  if FileExists(DataFileName) then
    Fish.LoadFromFile(DataFileName);
end;

procedure TForm19.FormDestroy(Sender: TObject);
begin
  Fish.MergeChangeLog;
  Fish.SaveToFile(DataFileName);
end;

function TForm19.Get(URL: string): string;
begin
  var Response := NetHTTPClient1.Get(URL);
  Result := Response.ContentAsString;
end;

procedure TForm19.GetFishImage;
begin
  var URL := BuildURL;
  Memo1.Lines.Add('----');
  Memo1.Lines.Add(URL);
  if URL = '' then
  begin

    Exit;
  end;
  var ResponseText := Get(URL);

  var Match := TRegEx.Match(ResponseText, '<meta property="og:image" content="(.*)"/>');
  if not Match.Success then
    Exit;

  var ImageURL := Match.Groups[1].Value;
  Memo1.Lines.Add('----');
  Memo1.Lines.Add(ImageURL);
//  ImageURL := StringReplace(ImageURL, '1200px', '400px', []);

  var ms : TMemoryStream;
  ms := TMemoryStream.Create;
  NetHTTPClient1.Get(ImageURL, ms);
  ms.Position := 0;
  ms.SaveToFile('c:\temp\fish\' + FishSpeciesName.AsString + '.jpg');
  try
    Fish.Edit;
    FishGraphic.LoadFromStream(ms);
  except
    FishGraphic.Clear;
    Fish.Cancel;
  end;
  Fish.Post;
end;

procedure TForm19.Log(const s: string);
begin
  memo1.Lines.Add(s);
end;

class function TImprovedJPEG.CanLoadFromStream(Stream: TStream): Boolean;
var
  soi, marker: Word;
  P: Int64;
begin
  P := Stream.Position;
  try
    Result := (Stream.Read(soi, SizeOf(soi)) = SizeOf(soi)) and (soi = $D8FF) and
      (Stream.Read(marker, SizeOf(marker)) = SizeOf(marker)) and ((marker and $E0FF = $E0FF) or (marker = $DBFF));
  finally
    Stream.Position := P;
  end;
end;

end.
